Builds The Book of Wyatt http://thebookofwyatt.com/

Uses jekyll-book-theme: https://github.com/henrythemes/jekyll-book-theme

## Setup A Build Environment
* Setup Jekyll https://jekyllrb.com/docs/installation/
* Clone this project
* Run `jekyll build` in the project to build to the _site/ directory.

## Adding A Chapter
* Create a markdown file in the `_chapters` directory
* Specify a title at the top  like this:
```
---
title: Wyatt
---
```
* Write the quotes!