---
title: Wyatt
---

“You can fork my stuff if you want.”

“That’s supposed to be ‘stroke,’ which I just had.”

“When the time comes that you let me see your flower…”

“What’s going to be on the midterm? Honestly, I don’t have a clue.”

“I should’ve been a nurse.”

“Has anyone done matrix multiplication? It’s nothing like that.”

“I don’t want to know the magic, I want to know what the hell’s going on!”

“I did it in a way so that it would work.” – Wyatt <br />
“That’s a good way to do it.” - Drew

“Where’s Biggle?”

“I’m kinda salty about this, but not wholly.”

“It just became a dudefest.”

“I don’t like people who touch my keyboard.”

“So I was kneeling in front of this girl’s desk slapping it, and this girl’s really nice, she went to Catholic school, and I was like ‘Isn’t this what you do at Catholic school?’”

“I don’t know if you guys are doing this, but we have five minutes left, so I might as well talk about it.”

“Think about where you’re thinking…”

“Wouldn’t it be amazing if something came up, like a dinosaur?”

“You must adhere to strict rules that we will make up as we go along.” – Requirements for Place Project

“That’s not a common goal, that’s just bullshit.”

“I’ll put you through the paddle machine.”

“I don’t know what magenta is. Is that a green?”

“Have you guys heard about God?”

“If you had dim lighting and bongos and more coffee, it’d be great.”

“It’s ten seconds of play after five minutes of bullshit.” – in CIS 375

“You thought it was a cake because you couldn’t hear the flag.”

“His name was Richard Dodson… and he was storming around the bridge yelling ‘Fuck the communists! Fuck the communists!’”